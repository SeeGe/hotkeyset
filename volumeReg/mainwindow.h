#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <windows.h>
#include <QSystemTrayIcon>

static void analyzeKeyCode(DWORD, DWORD);

static DWORD keyCode;
static DWORD keyFlag;

static bool shiftState;


static double volume;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();



private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

    void minimizeInTray();

    void changeEvent(QEvent* );

    void showTraymenu(QSystemTrayIcon::ActivationReason);
private:
    Ui::MainWindow *ui;



    //LRESULT CALLBACK MyLowLevelKeyBoardProc(int nCode, WPARAM wParam, LPARAM lParam);



    typedef int (*setHook)(HWND,UINT);
    typedef int (*unsetHook)();

    //int setHook(HWND,UINT);
    int unSetHook();

    //double volume;

    QSystemTrayIcon *trayIcon;
    QMenu *trayMenu;
    QAction *trayActionOpen;
    QAction *trayActionExit;
    QAction *trayActionParam;


    void acticateKeyLogger();

};

#endif // MAINWINDOW_H
