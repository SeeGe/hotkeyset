#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <mmdeviceapi.h>
#include <endpointvolume.h>
#include <Objbase.h>
#include <QDebug>
#include <QLibrary>
#include <QTime>
#include <QChar>
#include <Windows.h>
#include <iostream>

HHOOK hHook = NULL;

using namespace std;

static bool ChangeVolume(double nVolume,bool bScalar);
static double getCurrentVolume();

void analyzeKeyCode(DWORD code, DWORD flags)
{
    qDebug() << code << flags;
    if (code == 164 && flags == 32)
        shiftState = 1;
    else if (code == 164 && flags == 128)
        shiftState = 0;
    qDebug() << "shift" << shiftState;

    if (shiftState == 1 && code == 38)
    {
        qDebug() << "volumeUp";

        volume = getCurrentVolume() + 1;
        qDebug() << getCurrentVolume();

        ChangeVolume(volume, 0);
    }
    if (shiftState == 1 && code == 40)
    {
        qDebug() << "volumeDown";
        volume = getCurrentVolume() - 1;
        qDebug() << getCurrentVolume();

        ChangeVolume(volume, 0);
    }
}

void UpdateKeyState(BYTE *keystate, int keycode)
{
    keystate[keycode] = GetKeyState(keycode);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    acticateKeyLogger();


    volume = 0;

    volume = getCurrentVolume();

    qDebug() << volume;
    ChangeVolume(volume, 0);

    trayIcon = new QSystemTrayIcon(this);
    trayMenu = new QMenu;
    trayActionOpen = new QAction("Открыть", trayMenu);
    trayActionExit = new QAction("Выход", trayMenu);
    QList<QAction *> list;
    list.append(trayActionOpen);
    list.append(trayActionExit);
    trayMenu->addActions(list);
    trayIcon->setContextMenu(trayMenu);

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(showTraymenu(QSystemTrayIcon::ActivationReason)));
    connect(trayActionExit, SIGNAL(triggered()), this ,SLOT(close()));
    connect(trayActionOpen, SIGNAL(triggered()), this ,SLOT(showNormal()));

    this->setWindowState(Qt::WindowMinimized);
    this->hide();


}

LRESULT CALLBACK MyLowLevelKeyBoardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    //WPARAM is WM_KEYDOWN, WM_KEYUP, WM_SYSKEYDOWN, or WM_SYSKEYUP
    //LPARAM is the key information

    //Get the key information
    KBDLLHOOKSTRUCT cKey = *((KBDLLHOOKSTRUCT*)lParam);

    wchar_t buffer[5];

    //get the keyboard state
    BYTE keyboard_state[256];
    GetKeyboardState(keyboard_state);
    UpdateKeyState(keyboard_state, VK_SHIFT);
    UpdateKeyState(keyboard_state, VK_CAPITAL);
    UpdateKeyState(keyboard_state, VK_CONTROL);
    UpdateKeyState(keyboard_state, VK_MENU);

    //Get keyboard layout
    HKL keyboard_layout = GetKeyboardLayout(0);

    //Get the name
    char lpszName[0x100] = {0};

    DWORD dwMsg = 1;
    dwMsg += cKey.scanCode << 16;
    dwMsg += cKey.flags << 24;

    int i = GetKeyNameText(dwMsg, (LPTSTR)lpszName,255);

    //Try to convert the key info
    int result = ToUnicodeEx(cKey.vkCode, cKey.scanCode, keyboard_state, buffer,4,0, keyboard_layout);
    buffer[4] = L'\0';

    //Print the output
    qDebug() << "key: " << cKey.vkCode << " " << QString::fromUtf16((ushort*)buffer) << " " << QString::fromUtf16((ushort*)lpszName);
    QString line;
    int j = 0;
    for (int i = 0; i < 256; i++)
        j += keyboard_state[i];
    qDebug() << cKey.flags;

    analyzeKeyCode(cKey.vkCode, cKey.flags);

    return CallNextHookEx(hHook, nCode, wParam, lParam);
}

void MainWindow::acticateKeyLogger()
{
    hHook = SetWindowsHookEx(WH_KEYBOARD_LL, MyLowLevelKeyBoardProc, NULL, 0);
    if(hHook == NULL)
    {
        qDebug() << "Hook failed";
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::showTraymenu(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason)
    {
    case QSystemTrayIcon::Context:
        trayMenu->setFixedSize(100,50);
        QPoint pos = QCursor::pos();
        pos.setX(pos.x());
        pos.setY(pos.y()-50);
        trayMenu->exec(pos);
    }
}

double getCurrentVolume()
{

    HRESULT hr=NULL;

    CoInitialize(NULL);
    IMMDeviceEnumerator *deviceEnumerator = NULL;
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER,
                          __uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
    IMMDevice *defaultDevice = NULL;

    hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
    deviceEnumerator->Release();
    deviceEnumerator = NULL;

    IAudioEndpointVolume *endpointVolume = NULL;
    hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume),
         CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
    defaultDevice->Release();
    defaultDevice = NULL;

    // -------------------------
    float currentVolume = 0;
    endpointVolume->GetMasterVolumeLevel(&currentVolume);
    //printf("Current volume in dB is: %f\n", currentVolume);
    CoUninitialize();

    return (double)currentVolume;
}

bool ChangeVolume(double nVolume,bool bScalar)
{

    HRESULT hr=NULL;
    bool decibels = false;
    bool scalar = false;
    double newVolume=nVolume;

    CoInitialize(NULL);
    IMMDeviceEnumerator *deviceEnumerator = NULL;
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER,
                          __uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
    IMMDevice *defaultDevice = NULL;

    hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
    deviceEnumerator->Release();
    deviceEnumerator = NULL;

    IAudioEndpointVolume *endpointVolume = NULL;
    hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume),
         CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
    defaultDevice->Release();
    defaultDevice = NULL;

    // -------------------------
    float currentVolume = 0;
    endpointVolume->GetMasterVolumeLevel(&currentVolume);
    //printf("Current volume in dB is: %f\n", currentVolume);

    hr = endpointVolume->GetMasterVolumeLevelScalar(&currentVolume);
    //CString strCur=L"";
    //strCur.Format(L"%f",currentVolume);
    //AfxMessageBox(strCur);

    // printf("Current volume as a scalar is: %f\n", currentVolume);
    if (bScalar==false)
    {
        hr = endpointVolume->SetMasterVolumeLevel((float)newVolume, NULL);
    }
    else if (bScalar==true)
    {
        hr = endpointVolume->SetMasterVolumeLevelScalar((float)newVolume, NULL);
    }
    endpointVolume->Release();

    CoUninitialize();

    return FALSE;
}

void MainWindow::on_pushButton_clicked()
{
    volume = getCurrentVolume() - 1;
    qDebug() << getCurrentVolume();

    ChangeVolume(volume, 0);

}

void MainWindow::on_pushButton_2_clicked()
{
    volume = getCurrentVolume() + 1;
    qDebug() << getCurrentVolume();

    ChangeVolume(volume, 0);
}

void MainWindow::changeEvent(QEvent *e)
{
    if (e->type() == QEvent::WindowStateChange)
    {
        if (isMinimized())
        {
            trayIcon->setIcon(QIcon(QPixmap(":/icon/volIcon.png")));
            trayIcon->show();
            this->hide();
        } else if (isMaximized())
        {
            trayIcon->hide();
        }
    }
}

void MainWindow::minimizeInTray()
{

}

