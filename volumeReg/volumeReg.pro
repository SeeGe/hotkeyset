#-------------------------------------------------
#
# Project created by QtCreator 2015-12-09T21:34:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = volumeReg
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui


LIBS += -lOle32 \
        -lUser32

RESOURCES += \
    icons.qrc
